FROM ubuntu:latest
MAINTAINER frisbeeman

RUN apt-get update && apt-get install -y \
    python-pip \
    python-matplotlib \
#    python-matplotlib-doc \
    python-pandas \
#    python-pandas-doc \
    python-seaborn \
#    texlive-full \
#    python-sklearn-doc \
    python-sklearn

RUN pip install --upgrade pip && pip install jupyter \
    ipyleaflet \
    bqplot \
    pythreejs \
    tensorflow \
    xgboost

RUN jupyter nbextension enable --py --sys-prefix widgetsnbextension
RUN jupyter nbextension enable --py --sys-prefix pythreejs
RUN jupyter nbextension enable --py --sys-prefix ipyleaflet
RUN jupyter nbextension enable --py --sys-prefix bqplot

EXPOSE 80

RUN mkdir /tmp/jupyter
VOLUME /tmp/jupyter
WORKDIR /tmp/jupyter

ENV TINI_VERSION v0.13.2
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN chmod +x /tini
ENTRYPOINT ["/tini", "--"]

CMD ["jupyter", "notebook", "--port=80", "--no-browser", "--ip=*"]
